import Vue from 'vue'
import VueRouter from 'vue-router'
import Movies from '../features/movie/Main.vue'
import MovieForm from '../features/movie/components/MovieForm'
import Actors from '../features/actor/DisplayActors.vue'
import Producers from '../features/producer/DisplayProducers.vue'
import Genres from '../features/genre/DisplayGenres.vue'

Vue.use(VueRouter)

const routes = [{
        path: '',
        redirect: 'movies',

    }, {
        path: '/movies',
        name: 'Movies',
        component: Movies,
    },
    {
        path: '/movies/add',
        name: 'Add',
        component: MovieForm
    },
    {
        path: '/movies/edit/:id',
        name: 'Edit',
        component: MovieForm
    },
    {
        path: '/actors',
        name: 'Actors',
        component: Actors,
    },
    {
        path: '/producers',
        name: 'Producers',
        component: Producers,
    },
    {
        path: '/genres',
        name: 'Genres',
        component: Genres,
    },
    { path: '*', redirect: '/' }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router