export class MovieApiService {
    constructor() {}

    async getAll(page, size) {
        try {
            // let response = await axios.get(`${this.baseUrl}/movies`);
            let response = await axios.get("https://localhost:5001/api/movies", {
                params: {
                    pageNumber: page,
                    pageSize: size
                }
            });

            return response.data;
        } catch (err) {
            console.log(err);
        }
    }

    async add(data) {
        try {
            await axios.post("https://localhost:5001/api/movies", data);
        } catch (err) {
            console.log(err);
        }
    }

    async getById(id) {
        try {
            let response = await axios.get(`https://localhost:5001/api/movies/${id}`);
            return response.data;
        } catch (err) {
            console.log(err);
        }
    }
    async update(id, data) {
        try {
            await axios.put(`https://localhost:5001/api/movies/${id}`, data);
        } catch (err) {
            console.log(err);
        }
    }

    async delete(id) {
        try {
            await axios.delete(`https://localhost:5001/api/movies/${id}`);
        } catch (err) {
            console.log(err);
        }
    }


}