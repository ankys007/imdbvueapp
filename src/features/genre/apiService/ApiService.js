export class GenreApiService {
    constructor() {}

    async getAll() {
        try {
            let response = await axios.get("https://localhost:5001/api/genres");
            return response.data;
        } catch (err) {
            console.log(err);
        }
    }

    async add(data) {
        try {
            let response = await axios.post("https://localhost:5001/api/genres", data);
            return response.data;
        } catch (err) {
            console.log(err);
        }
    }
    async getById(id) {
        try {
            let response = await axios.get(`https://localhost:5001/api/genres/${id}`);
            return response.data;
        } catch (err) {
            console.log(err);
        }
    }
    async update(id, data) {
        try {
            await axios.put(`https://localhost:5001/api/genres/${id}`, data);
        } catch (err) {
            console.log(err);
        }
    }
    async delete(id) {
        try {
            await axios.delete(`https://localhost:5001/api/genres/${id}`)
        } catch (err) {
            console.log(err);
        }
    }
}